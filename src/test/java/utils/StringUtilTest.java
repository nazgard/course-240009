package utils;

import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class StringUtilTest {

    private String input = "hello hello world comrade jetty yellow a b c  \r\n";
    private String separator = " ";

    private final String HELLO_STRING_TIMES = "hello word should encountered 2 times";
    private final String WORLD_STRING_TIMES = "hello word should encountered 1 time";
    private final String SPEC_SYMBOLS_ENCOUNTERED = "should be only printable symbols";
    private final String PIES_COUNT = "pies should be 5";


    @Test
    public void testEncounteredWords() throws Exception {

        StringUtil stringUtil = new StringUtil();
        Map<String,Integer> testMap = stringUtil.encounteredWords(input, separator);
        assertEquals(HELLO_STRING_TIMES, (Integer) 2, testMap.get("hello"));
        assertEquals(WORLD_STRING_TIMES, (Integer) 1, testMap.get("world"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get("\r"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get("\n"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get(" "));

        testMap = stringUtil.encounteredWords(input);
        assertEquals(PIES_COUNT, testMap.size(), 5);
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get("a"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get("b"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get("c"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get("\r"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get("\n"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get(" "));
    }

    @Test
    public void testMakePieData() throws Exception {
        //TODO поправить индексы
        StringUtil stringUtil = new StringUtil();
        Map<String,Integer> testMap = stringUtil.encounteredWords(input);
        ObservableList<PieChart.Data> observableList = stringUtil.makePieData(testMap, 5);

        assertEquals(PIES_COUNT, 5, observableList.size());
        assertEquals(2, observableList.get(0).getPieValue(), 0);
        assertEquals(1, observableList.get(1).getPieValue(), 0);
        assertEquals(1, observableList.get(2).getPieValue(), 0);
        assertEquals(1, observableList.get(3).getPieValue(), 0);
        assertEquals(1, observableList.get(4).getPieValue(), 0);

        testMap = stringUtil.encounteredSymbols(input);
        observableList = stringUtil.makePieData(testMap, 5);
        assertEquals(PIES_COUNT, 5, observableList.size());
        assertEquals(7, observableList.get(0).getPieValue(), 0);
        assertEquals(5, observableList.get(1).getPieValue(), 0);
        assertEquals(5, observableList.get(2).getPieValue(), 0);
        assertEquals(2, observableList.get(3).getPieValue(), 0);
        assertEquals(2, observableList.get(4).getPieValue(), 0);
    }

    @Test
    public void testEncounteredSymbols() throws Exception {
        StringUtil stringUtil = new StringUtil();
        Map<String,Integer> testMap = stringUtil.encounteredSymbols(input);

        assertEquals(HELLO_STRING_TIMES, (Integer) 7, testMap.get("l"));
        assertEquals(WORLD_STRING_TIMES, (Integer) 5, testMap.get("o"));
        assertEquals(WORLD_STRING_TIMES, (Integer) 5, testMap.get("e"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get("\r"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get("\n"));
        assertEquals(SPEC_SYMBOLS_ENCOUNTERED, null, testMap.get(" "));
    }
}