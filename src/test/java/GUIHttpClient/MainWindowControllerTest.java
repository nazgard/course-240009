package GUIHttpClient;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

public class MainWindowControllerTest {
    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    @Ignore
    public void testExit() throws Exception {
        //TODO make exit test
        exit.expectSystemExitWithStatus(0);
    }
}