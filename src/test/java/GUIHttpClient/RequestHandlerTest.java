package GUIHttpClient;

import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.junit.Test;

import static org.junit.Assert.*;

public class RequestHandlerTest {

    //TODO сделать mock объекты, чтобы не зависить от интернета

    @Test
    public void testGetHeadersHtml() throws Exception {
        RequestHandler requestHandler = new RequestHandler("http://www.e1.ru");
        Header[] headers = requestHandler.getHeaders();
        String content = requestHandler.getContent();
        StatusLine statusLine = requestHandler.getStatusLine();
        String json = requestHandler.getJson();

        assertEquals("Status code should be 200", 200, statusLine.getStatusCode());
        assertEquals("Status phrase should be OK", "OK", statusLine.getReasonPhrase());
        assertNotNull(headers);
        assertEquals("Main have html rather than json", "", json);
        assertNotNull(content);
    }

    @Test
    public void testGetHeadersJson() throws Exception {
        RequestHandler requestHandler = new RequestHandler("http://jsonplaceholder.typicode.com/users/1");
        Header[] headers = requestHandler.getHeaders();
        String content = requestHandler.getContent();
        StatusLine statusLine = requestHandler.getStatusLine();
        String json = requestHandler.getJson();

        assertEquals("Status code should be 200", 200, statusLine.getStatusCode());
        assertEquals("Status phrase should be OK", "OK", statusLine.getReasonPhrase());
        assertNotNull(headers);
        assertEquals("First user data isn't valid", "" +
                "{\n" +
                "  \"id\": 1,\n" +
                "  \"name\": \"Leanne Graham\",\n" +
                "  \"username\": \"Bret\",\n" +
                "  \"email\": \"Sincere@april.biz\",\n" +
                "  \"address\": {\n" +
                "    \"street\": \"Kulas Light\",\n" +
                "    \"suite\": \"Apt. 556\",\n" +
                "    \"city\": \"Gwenborough\",\n" +
                "    \"zipcode\": \"92998-3874\",\n" +
                "    \"geo\": {\n" +
                "      \"lat\": \"-37.3159\",\n" +
                "      \"lng\": \"81.1496\"\n" +
                "    }\n" +
                "  },\n" +
                "  \"phone\": \"1-770-736-8031 x56442\",\n" +
                "  \"website\": \"hildegard.org\",\n" +
                "  \"company\": {\n" +
                "    \"name\": \"Romaguera-Crona\",\n" +
                "    \"catchPhrase\": \"Multi-layered client-server neural-net\",\n" +
                "    \"bs\": \"harness real-time e-markets\"\n" +
                "  }\n" +
                "}", json);
        assertNotNull(content);
    }
}