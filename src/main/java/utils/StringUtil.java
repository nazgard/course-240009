package utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class StringUtil {

    public Map<String,Integer> encounteredWords(String input){
        return encounteredWords(input, "\\s*[^a-zA-Z]+\\s*");
    }

    public Map<String,Integer> encounteredWords(String input, String regex){

        String[] inputStringsArray = input.split(regex);

        Map<String,Integer> map = new HashMap<>();

        for (String anInputStringsArray : inputStringsArray) {
            String s = anInputStringsArray.trim();
            if (!s.isEmpty() && s.matches("\\w{3,}")) {
                if (map.get(s) == null) {
                    map.put(s, 1);
                }else {
                    Integer count = map.get(s);
                    map.put(s, ++count);
                }
            }
        }


        return map;
    }

    public Map<String,Integer> encounteredSymbols(String input) {

        char[] inputCharsArray = input.toCharArray();

        Map<String,Integer> map = new HashMap<>();

        for (char c : inputCharsArray) {
            String valueOfChar = String.valueOf(c).trim();
            if (!valueOfChar.isEmpty()){
                if (map.get(valueOfChar) == null) {
                    map.put(valueOfChar, 1);
                }else {
                    Integer count = map.get(valueOfChar);
                    map.put(valueOfChar, ++count);
                }
            }
        }


        return map;
    }

    public ObservableList<PieChart.Data> makePieData(Map<String,Integer> input, Integer count) {

        return input.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .limit(count)
                .map(entry -> new PieChart.Data(entry.getKey(), entry.getValue()))
                .collect(Collectors.toCollection(FXCollections::observableArrayList));
    }
}
