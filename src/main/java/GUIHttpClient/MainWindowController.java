package GUIHttpClient;

import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import utils.StringUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

public class MainWindowController {

    public TextArea header_content_text;
    public TextField urlField;
    public TextArea json_content_text;
    public ProgressBar progress_bar;
    public TextArea raw_content_text;
    public Tab json_tab;
    public PieChart pie_top_words;
    public Tab charts_tab;
    public PieChart pie_top_symbols;
    public Tab header_tab;
    public Tab raw_tab;

    public void exit(){
        System.exit(0);
    }

    public void ActionBtnClick() throws IOException {
        StringUtil stringUtil = new StringUtil();

        RequestService service = new RequestService();
        service.setUrl(urlField.getText());
        service.setOnSucceeded(t -> {
            Map response = (Map) t.getSource().getValue();
            String headers = response.get("header").toString();
            header_tab.setDisable(headers.isEmpty());
            header_content_text.setText(headers);

            String json = response.get("json").toString();
            json_tab.setDisable(json.isEmpty());
            json_content_text.setText(response.get("json").toString());

            String content = response.get("content").toString();
            raw_tab.setDisable(content.isEmpty());
            raw_content_text.setText(content);

            Integer contentEncounteredWordCount = 5;
            ObservableList<PieChart.Data> pieWordsData = stringUtil.makePieData(stringUtil.encounteredWords(content), contentEncounteredWordCount);
            ObservableList<PieChart.Data> pieSymbolsData = stringUtil.makePieData(stringUtil.encounteredSymbols(content), contentEncounteredWordCount);
            charts_tab.setDisable(pieWordsData.size() == 0);
            pie_top_words.setData(pieWordsData);
            pie_top_words.setTitle("Топ " + contentEncounteredWordCount + " часто встречающихся слов");
            charts_tab.setDisable(pieSymbolsData.size() == 0);
            pie_top_symbols.setData(pieSymbolsData);
            pie_top_symbols.setTitle("Топ " + contentEncounteredWordCount + " часто встречающихся символов");
        });

        progress_bar.progressProperty().bind(service.progressProperty());
        service.start();
    }

    public void help() {
        final WebView webView = new WebView();

        URL url = getClass().getResource("/help/help.htm");
        webView.getEngine().load(url.toString());

        Stage stage = new Stage();
        stage.setScene(new Scene(webView));
        stage.setTitle("Помощь");
        stage.show();
    }

    public void saveToFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Сохранение ответа сервера");
        fileChooser.setInitialFileName("response.txt");
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try {
                saveFileRoutine(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveFileRoutine(File file) throws IOException {
        String fileContent = "";
        fileContent += "URL: " + urlField.getText();
        fileContent += "\n\n\n\n";
        fileContent += header_content_text.getText();
        fileContent += "\n\n\n\n";
        fileContent += raw_content_text.getText();

        if (file.createNewFile()) {
            FileWriter writer = new FileWriter(file);
            writer.write(fileContent);
            writer.close();
        }
    }
}
