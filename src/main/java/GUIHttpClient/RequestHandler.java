package GUIHttpClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class RequestHandler {

    private StatusLine statusLine;
    private Header[] headers;
    private String content = "";
    private String json = "";


    public RequestHandler(String url) throws IOException {

        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        CloseableHttpResponse response = client.execute(request);
        HttpEntity httpEntity = response.getEntity();

        this.headers = response.getAllHeaders();
        this.statusLine = response.getStatusLine();

        String content = parseContent(httpEntity);
        String JSON = parseJSON(httpEntity, content);

        setContent(content);
        setJson(JSON);
    }

    private String parseContent(HttpEntity httpEntity) throws IOException {

        return EntityUtils.toString(httpEntity, "UTF-8");
    }

    private String parseJSON(HttpEntity httpEntity, String content) {

        String prettyJsonString = "";
        if( httpEntity.getContentType().toString().toLowerCase().contains("application/json") ){
            try {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonParser jp = new JsonParser();
                JsonElement je = jp.parse(content);
                prettyJsonString = gson.toJson(je);
            }catch (Exception ex){
                prettyJsonString = "Получен невалидный JSON";
            }
        }

        return prettyJsonString;
    }


    public Header[] getHeaders() {
        return headers;
    }

    public String getContent() {
        return content;
    }

    private void setContent(String content) {
        this.content = content;
    }

    public StatusLine getStatusLine() {
        return statusLine;
    }

    public String getJson() {
        return json;
    }

    private void setJson(String json) {
        this.json = json;
    }
}
