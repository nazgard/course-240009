package GUIHttpClient;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.apache.http.Header;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RequestService extends Service<Map> {

    private String url;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    protected Task<Map> createTask() {
        return new Task<Map>() {
            @Override
            protected Map<String, String> call() throws IOException {
                RequestHandler requestHandler = new RequestHandler(getUrl());

                Header[] headers = requestHandler.getHeaders();
                String content = requestHandler.getContent();
                String h = "";
                String json = requestHandler.getJson();

                for (Header header : headers) {
                    h += header.getName() + ": " + header.getValue() + "\n";
                }

                Map<String, String> result = new HashMap<>();
                result.put("header", h);
                result.put("json", json);
                result.put("content", content);

                updateProgress(0,0);

                return result;
            }
        };
    }
}
